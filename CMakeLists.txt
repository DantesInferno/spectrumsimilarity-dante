# This is an example project to show and test the usage of the FindOpenCL
# script.


cmake_minimum_required( VERSION 3.1.0 FATAL_ERROR )
set (CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

project( SpectrumSimilarity )

set(CMAKE_MODULE_PATH "${CMAKE_SOURCE_DIR}/cmake")
set(EXECUTABLE_OUTPUT_PATH bin)
set(LIBRARY_OUTPUT_PATH bin)

include_directories( "${CMAKE_SOURCE_DIR}/khronos" )

## Compiler flags
if(CMAKE_COMPILER_IS_GNUCXX)
    set(CMAKE_CXX_FLAGS "-O2")        ## Optimize
    set(CMAKE_EXE_LINKER_FLAGS "-s")  ## Strip binary
endif()

SUBDIRS(modules)

enable_testing()
