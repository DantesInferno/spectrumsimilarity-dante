# README #
---

## Description
* This is a starting project for Dante in Fiehn Lab.
* It accelerates the spectrum similarity computation using OpenCL, and compare performance improvement
on both CPU and GPU.
* Purpose: offer APIs for a rest server in Fiehn Lab.

## How do I set up

### Requirements

to compile the code and build the application, please ensure you have the following software tools installed

cmake
make
gcc/g++
OpenCL bindings

and the following libraries

gtest
openssl
openssl crypto

### Building the code

* clone this directory

```git clone https://DantesInferno@bitbucket.org/DantesInferno/spectrumsimilarity-dante.git```

* enter: cmake ./ in the main directory
* enter: make clean && make

This should build the application and all the required libraries.

### starting the rest server

The similarity REST server will be located at modules/similarity-rest-search/bin

provides the following endpoints

* POST similarity/add, this will add a spectra to the library, used for calculation
* PUT similarity/commit, this will commit all spectra to the graphic card's memory
* POST similarity/search, will execute a similarity search
* GET similarity/librarySize, will return the size of the current library size

## Clients

We provide a handful of clients to simplify the integration with this REST Api and computing library.

* python REST [client](https://bitbucket.org/fiehnlab/pyossa)
* java REST [client](https://bitbucket.org/fiehnlab/wcmc)
* R REST [client](https://bitbucket.org/fiehnlab/ossa-r)

## Who do I talk to?
* Gert Wohlgemuth
* Sajjan Singh Mehta

## Note
* Dataset is from ~242k NIST spectra