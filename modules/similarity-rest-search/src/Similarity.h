//
// Created by Gert Wohlgemuth on 6/27/17.
//

#ifndef SPECTRUMSIMILARITY_SIMILARITY_H
#define SPECTRUMSIMILARITY_SIMILARITY_H

#include <string>
#include <ngrest/common/Service.h>
#include <SimilaritySearch.h>
#include <list>
#include "Result.h"

//
// * location: similarity
//
class Similarity : public ngrest::Service {
public:
    //
    // *method: GET
    // *location: librarySize
    //
    long librarySize();

    //
    // *method: PUT
    // *location: commit
    //
    void commit();

    //
    // *method: POST
    // *location: /add
    //
    void add(const UploadRequest upload);

    //
    // *method: POST
    // *location: /clear
    //
    void clear();

    //
    // *method: POST
    // *location: /search
    //
    //
    std::list<Result> similaritySearch(const SearchRequest searchRequest);

    Similarity();

    ~Similarity();

private:
    SimilaritySearch* search;

};

#endif //SPECTRUMSIMILARITY_SIMILARITY_H
