//
// Created by Gert Wohlgemuth on 6/27/17.
//

#include <signal.h>
#include <iostream>

#include <ngrest/utils/Log.h>
#include <ngrest/engine/Engine.h>
#include <ngrest/engine/ServiceDispatcher.h>
#include <ngrest/engine/Deployment.h>
#include <ngrest/engine/HttpTransport.h>

#include "servercommon.h"
#include "Server.h"
#include "ClientHandler.h"

#include "ServiceGroupImpl.h"

#if defined WIN32 || defined __APPLE__
typedef void(__cdecl *sighandler_t)(int);
#endif

int main(int argc, char* argv[])
{
    ngrest::StringMap args = {
       {"p", "8080"} // listen port
       //, {"l", "127.0.0.1"} // listen on specific IP, default: all
    };

    static ngrest::Server server;
    ngrest::ServiceDispatcher dispatcher;
    ngrest::Deployment deployment(dispatcher);
    ngrest::HttpTransport transport;
    ngrest::Engine engine(dispatcher);
    ngrest::ClientHandler clientHandler(engine, transport);

    server.setClientCallback(&clientHandler);

    if (!server.create(args))
        return 1;

    sighandler_t signalHandler = [] (int) {
        ngrest::LogInfo() << "Stopping server";
        server.quit();
    };

    ::signal(SIGINT, signalHandler);
    ::signal(SIGTERM, signalHandler);


    ServiceGroupImpl group;
    deployment.deployStatic(&group);

    return server.exec();
}
