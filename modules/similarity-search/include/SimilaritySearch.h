//
// Created by wohlgemuth on 6/19/17.
//

#ifndef PROJECT_SIIMILARITYSEARCH_H
#define PROJECT_SIIMILARITYSEARCH_H

#include <vector>
#include <memory>
#include "SimilarityResult.h"
#include "Spectrum.h"
#include "../cl.hpp"
#include "Util.h"

using namespace std;

#define COSINE_SIMILARITY_KERNEL "compute_cosine"
#define COMPOSITE_SIMILARITY_KERNEL "compute_composite"
#define MAX_ION_COUNT_FOR_SPECTRA   1000

/**
 * executes a similarity search against the GPU api
 */
class SimilaritySearch {

public:

    /**
     * deconstructor
     */
    ~SimilaritySearch();

    /**
     * we are using by default the first detected CPU in the system for our calculations
     */
    SimilaritySearch();

    /**
     * we are using by default the first detected CPU in the system for our calculations
     */
    SimilaritySearch(string kernelFile);

    /**
     * allows us to specify which exact device we would like to use
     * @param device
     */
    SimilaritySearch(vector<cl::Device> &devices);


    /**
     * returns the size of our library
     * @return
     */
    unsigned long getLibrarySize();

    /**
     * adds a new library spectrum to our internal storage
     * @param librarySpectrum
     */
    void addLibrarySpectra(const string librarySpectrum);

    /**
     * adds a library spectra with a specfic id
     * @param id
     * @param librarySpectrum
     */
    void addLibrarySpectra(const  string id, const string librarySpectrum);

    /**
     * loads all the spectra in the given file, into our internal storage
     * @param fileName
     */
    void loadLibrarySpectra(const string fileName);

    /**
     * commits the recently added spectra to the memory of the used device
     */
    void commitAddedSpectra();

    /**
     * clears the internal library
     */
    void clear();

    /**
     * executes the actual search, against the library
     * @param unknown the unknown spectrum
     * @param minimumSimilarity our minimum required similarity
     * @return a vector containing all our search results
     */
    vector<SimilarityResult> search(const string unknown,float minimumSimilarity);

    /**
     * takes a given spectrum and executes a similarity search against the
     * internal storage
     * @param unknown
     * @param minimumSimilarity
     * @return
     */
    vector<SimilarityResult> search(Spectrum unknown,float minimumSimilarity);
protected:

    /**
     * allows us to specify the device and the actual used kernel file
     * for this
     * @param devcice
     * @param kernelFile
     */
    SimilaritySearch(vector<cl::Device> &devices, string kernelFile);

    /**
     * does the actual initialization for us
     * @param kernelFile
     */
    void init(vector<cl::Device> &devices, string kernelFile);

private:

    /**
     * internal storage of our build library
     */
    vector<Spectrum> library;

    /**
     * our kernel
     */
    shared_ptr<cl::Kernel> similarityKernel;

    /**
     * computes weighted intensities and is required for
     * similarity calculations
     */
    shared_ptr<cl::Kernel> weightedIntensityKernel;

    /**
     * computational queue
     */
    vector<cl::CommandQueue> queues;

    /**
     * simple buffer to hold our memory objects
     * should never exceed size 1 and ensures that we clean up
     * after ourself and don't run out of memory
     * on the card or so
     */
    vector<cl::Buffer> buffer;


    /**
     * little helper class
     */
    Util util;

    shared_ptr<cl::Context> context;
};


#endif //PROJECT_SIIMILARITYSEARCH_H
