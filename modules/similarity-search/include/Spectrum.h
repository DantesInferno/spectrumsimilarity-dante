//
// Created by Gert Wohlgemuth on 6/20/17.
//

#ifndef SPECTRUMSIMILARITY_SPECTRUM_H
#define SPECTRUMSIMILARITY_SPECTRUM_H


#include <string>
#include <map>

using namespace std;

/**
 * defines a spectrum in the system
 */
class Spectrum {

public:

    /**
     * constructor to actually define a spectra
     * @param spectra
     */
    Spectrum(const string id, const string spectra);

    /**
     * defines a new spectrum
     * @param spectra
     */
    Spectrum(const string spectra);


    /**
         * returns the unique splash indentifier
         * @return
         */
    string getSplash();

    /**
     * return the original spectra string as submitted to the system
     *
     * @return
     */
    string getOriginal();

    /**
     * returns the id of the spectra
     * @return
     */
    string getId();
    /**
     * returns the internal spectra representation
     */
    map<int, int> getSpectra();

private:

    /**
     * the original spectra string
     */
    string original;

    /**
     * the computed splash string
     */
    string splash;

    /**
     * the internal representation of the spectrum
     * mapped as integers for now
     *
     * obviously this does not support accurate mass spectra at this stage
     */
    map<int, int> spectra;

    /**
     * internal id of the spectra
     */
    string id;
};


#endif //SPECTRUMSIMILARITY_SPECTRUM_H
