//
// Created by Gert Wohlgemuth on 6/20/17.
//

#ifndef SPECTRUMSIMILARITY_UTIL_H
#define SPECTRUMSIMILARITY_UTIL_H
#include <string>
#include <map>
#include "../cl.hpp"
#include "Spectrum.h"

using namespace std;

/**
 * simple utility class to convert spectra for us to arrays, etc and ensure they can be tested properly
 */
class Util {

public:
    /**
     * calculate the weighted intensity for each ion
     * for now, this is simply the intensity value casted as an integer
     * @param mz ion m/z ratio
     * @param intensity ion intensity
     *
     * @return a map containing the masses as keys and intensities as values
     */
    double calculateWeightedIntensity(double mz, double intensity);

    /**
     * bin an m/z value in order to handle accurate masses
     * this is done by adding a factor of 0.2 and rounding down to the nearest int
     * @param mz ion m/z ratio
     *
     * @return a binned m/z value
     */
    int roundMZ(double mz);

    /**
     * converts the given input spring to a map of related ions and intensities
     * @param spectrum_string string in the following format: mz:int mz:int mz:int
     *
     * @return a map containing the masses as keys and intensities as values
     */
    map<int, int> convertSpectra(const string &spectrum_string);

    /**
     * a simple splitting function, based on the provided delimeter and the given input string
     *
     * @param s
     * @param delimeter
     * @return
     */
    vector<string> split(const string &s, char delimeter);

    /**
     * this copies the input vector to the defined array pointer (spec)
     *
     *
     * @param spectra all our spectra
     * @param spec array of spectra
     * @param numCols maximum size of spectra
     */
    void flattenToArray(vector<map<int, int>> &spectra, int *spec, int numCols);

    void flattenToArray(vector<Spectrum> &spectra, int *spec, int numCols);

    /**
     * returns all available devices or the specified type
     * @param platforms this pointer will contain all discovered platforms and devices
     * @param devices this pointer will contain all the found devices in your system
     * @param this is the type of device you are interrested in
     */
    void findDevice(vector<cl::Platform> &platforms,vector<cl::Device> &devices,cl_device_type type);

    void copySpectraToGpu(const vector<Spectrum> &spectra, int maxIonCount, cl::Kernel );
};


#endif //SPECTRUMSIMILARITY_UTIL_H
