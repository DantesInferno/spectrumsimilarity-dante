// Iris only supports float. uncomment this for other GPU
//#pragma OPENCL EXTENSION cl_khr_fp64 : enable


kernel void compute_composite(global int* intensity, global float* composite, global int* unknown, global int* numCols){
    
    //const int numCols = 1000; // number of columns subject to change
    size_t row_library;
    int index_library, sum, mul1, mul2, i, numUnknown, numSharedIrons, previousIntensity_Unknown, previousIntensity_Library;
    float docProduct, peakPairRatio, parenthesis, sum_parenthesis;
    
    //one work group per row
    row_library = get_global_id(0);
    numUnknown = 0; numSharedIrons = 0;
    previousIntensity_Unknown = 0; previousIntensity_Library = 0;
    sum = 0; mul1 = 0; mul2 = 0;
    numSharedIrons = 0; numUnknown = 0;
    sum_parenthesis = 0.0;
    parenthesis = 0; peakPairRatio = 0;
    
    //set the previous intensity of the first iron to be the intensity of itself
    for( i = 0; i < *numCols;){
        index_library = row_library * *numCols + i;
        if( intensity[index_library] > 0 && intensity[i] > 0 ){
            previousIntensity_Unknown = unknown[i];
            previousIntensity_Library = intensity[index_library];
            break;
        }
        i = i + 1;
    }
    
    for(i = 0; i < *numCols; i = i + 1){
        index_library = row_library * *numCols + i;
        
        if( unknown[i] > 0 || intensity[index_library] > 0 ){
            
            //computes Dot-Product
            sum += intensity[index_library] * unknown[i];
            mul1 += intensity[index_library] * intensity[index_library];
            mul2 += unknown[i] * unknown[i];
            
            //only cares about shared iron
            if( unknown[i] > 0 && intensity[index_library] > 0 ){
                numSharedIrons = numSharedIrons + 1;
                parenthesis = intensity[index_library] * 1.0 / previousIntensity_Library * previousIntensity_Unknown / unknown[i];
                
                // when the term in parentheses needs to be less than unity
                if( parenthesis > 1 ){
                    parenthesis = 1.0 / parenthesis;
                }
                
                sum_parenthesis = sum_parenthesis + parenthesis;
                previousIntensity_Unknown = unknown[i];
                previousIntensity_Library = intensity[index_library];
                
            }
            
            if( unknown[i]> 0){
                numUnknown = numUnknown + 1;
            }
        }
    }
    docProduct =  sum * 1.0 * sum / mul1 / mul2;
    if(numSharedIrons > 0)
        peakPairRatio = sum_parenthesis / numSharedIrons;
    float ans = ( numUnknown * docProduct + numSharedIrons * peakPairRatio ) / ( numUnknown + numSharedIrons );
    composite[row_library] = ans;
}
