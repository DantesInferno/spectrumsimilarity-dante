
// Iris only supports float.

kernel void compute_cosine(global int* input, global float* output, global int* unknown, global int* numCols){
    
    //const int numCols = 1000;
    size_t row_library = get_global_id(0);
    
    int index_library, sum, mul1, mul2;
    int i;
    
    sum = 0;
    mul1 = 0; mul2 = 0;
    for(i = 0; i < *numCols; i++){
        index_library = row_library * *numCols + i;
        
        sum += input[index_library] * unknown[i];
        mul1 += input[index_library] * input[index_library];
        mul2 += unknown[i] * unknown[i];
    
    }
    output[row_library] = (float) sum * sum / mul1  / mul2;
}
