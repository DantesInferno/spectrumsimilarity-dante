//
// Created by wohlgemuth on 6/19/17.
//

#include <fstream>
#include <iostream>
#include <iomanip>
#include "../include/SimilaritySearch.h"
#include "../../thirdparty/spectra-hash/cpp/src/splash.hpp"
#include "../include/weightedIntensity.h"
#include "../include/compute_cosine.h"
#include "../include/compute_composite.h"

unsigned long SimilaritySearch::getLibrarySize() {
    return this->library.size();
}


void SimilaritySearch::addLibrarySpectra(const string id, const string librarySpectrum) {
    //add this spectra to the internal database
    Spectrum spectrum(id, librarySpectrum);
    this->library.push_back(spectrum);

    //since the library was modified, clear it now
    this->buffer.clear();
}

void SimilaritySearch::addLibrarySpectra(const string librarySpectrum) {
    addLibrarySpectra(splashIt(librarySpectrum, '1'), librarySpectrum);
}

void SimilaritySearch::loadLibrarySpectra(const string fileName) {
    ifstream myReadFile;
    myReadFile.open(fileName);
    string line;

    ////cout << "loading file " + fileName + "\n";

    while (getline(myReadFile, line)) {

        //hash defines a comment in the file
        if (line.find("#") != 0) {
            ////cout << line + "\n";

            // Handle input in the following form: [id]\t[spectrum string]
            size_t delim_pos = line.find(',');

            if (delim_pos != std::string::npos) {
                std::string id = line.substr(0, delim_pos);
                std::string spectrum_string = line.substr(delim_pos + 1);
                addLibrarySpectra(id, spectrum_string);

            }
        }
    }

    myReadFile.close();

}

vector<SimilarityResult> SimilaritySearch::search(const string unknown, float minimumSimilarity) {
    return search(Spectrum(unknown), minimumSimilarity);
}

SimilaritySearch::SimilaritySearch(vector<cl::Device> &devices) {
    this->init(devices, COSINE_SIMILARITY_KERNEL);
}

SimilaritySearch::SimilaritySearch() {
    vector<cl::Platform> platforms;
    vector<cl::Device> devices;

    util.findDevice(platforms, devices, CL_DEVICE_TYPE_GPU);

    this->init(devices, COSINE_SIMILARITY_KERNEL);
}

SimilaritySearch::SimilaritySearch(string kernelFile) {
    vector<cl::Platform> platforms;
    vector<cl::Device> devices;

    util.findDevice(platforms, devices, CL_DEVICE_TYPE_GPU);

    this->init(devices, kernelFile);
}

SimilaritySearch::SimilaritySearch(vector<cl::Device> &devices, string kernelFile) {
    this->init(devices, kernelFile);
}

/**
 * TODO figure out how to run on multiple devices.
 *
 * @param devices
 * @param kernelFile
 */
void SimilaritySearch::init(vector<cl::Device> &devices, string kernelFile) {

    cl::Program::Sources sources;

    //build our sources for the program to be executed
//    //cout << "added to sources"<< endl;

    sources.push_back(make_pair(weightedIntensity_ocl, strlen(weightedIntensity_ocl)));

    if(kernelFile == COSINE_SIMILARITY_KERNEL){
        sources.push_back(make_pair(compute_cosine_ocl, strlen(compute_cosine_ocl)));
    }
    else if(kernelFile == COMPOSITE_SIMILARITY_KERNEL){
        sources.push_back(make_pair(compute_composite_ocl, strlen(compute_composite_ocl)));
    }
    else{
        cerr << "invalid kernel specified" << endl;
        exit(-1);
    }

//    //cout << "defining context" << endl;

    context = make_shared<cl::Context>(cl::Context(devices));

//    //cout << "building program" << endl;

    //compile our sources
    cl::Program program (*context.get(), sources);

    if (program.build({ devices }) != CL_SUCCESS)
    {
        std::cout<<" Error building: "<<program.getBuildInfo<CL_PROGRAM_BUILD_LOG>(devices.front())<<"\n";
        exit(-1);
    }

//    //cout << "creating queue"<<endl;

    /*
    //define 1 queue for each known device
    for(auto const& device:devices) {

        queues.push_back(cl::CommandQueue(*context.get(), device));
    }
     */

    queues.push_back(cl::CommandQueue(*context.get(), devices.front()));

    this->similarityKernel = make_shared<cl::Kernel>(cl::Kernel(program, kernelFile.c_str()));
    this->weightedIntensityKernel = make_shared<cl::Kernel>( cl::Kernel(program, "weightedIntensity"));

//    //cout << "finished init";
}

/**
 * TODO figure out how to utilize all available devices. One apprach would be to split the given data matrix into one matrix for each device. Right now we only work on the first found device
 */
void SimilaritySearch::commitAddedSpectra() {

//    //cout << "commiting spectra" << endl;

    int numCols = MAX_ION_COUNT_FOR_SPECTRA;
    int numRows = (int) this->getLibrarySize();

    int *spectra = new int[numRows * numCols];
    int *weightedIntensity = new int[numCols * numRows];

    memset(spectra, 0, sizeof(int) * numRows * numCols);
    memset(weightedIntensity, 0, sizeof(int) * numRows * numCols);

    util.flattenToArray(this->library, spectra, numCols);

    //free up potentially used old buffers
    buffer.clear();

//    //cout << "creating buffers" << endl;

    cl::Buffer buf_arr(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                       sizeof(int) * numRows * numCols, spectra);

    cl::Buffer buf_weightedIntensity(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                     sizeof(int) * numRows * numCols, weightedIntensity);

    this->weightedIntensityKernel->setArg(0, buf_arr);
    this->weightedIntensityKernel->setArg(1, buf_weightedIntensity);


//    //cout << "starting kernels" << endl;

    //compute the weights
    /*
     * TODO
     */
    //for(auto const& queue: queues) {
    //    queue.enqueueNDRangeKernel(*weightedIntensityKernel, cl::NullRange, cl::NDRange(numRows, numCols));
    //}

    //TODO right now we just run on the first device
    queues.front().enqueueNDRangeKernel(*weightedIntensityKernel, cl::NullRange, cl::NDRange(numRows, numCols));

    //store for later use
    buffer.push_back(buf_weightedIntensity);


//    //cout << "cleaning up" << endl;

    //memory error happens somehow here. somhehow use a shared pointer instead
    delete[] spectra;
    delete[] weightedIntensity;
//    //cout << "commited spectra to memory" << endl;

}

/**
 * todo make this work on multiple devices
 * @param unknown
 * @param minimumSimilarity
 * @return
 */
vector<SimilarityResult> SimilaritySearch::search(Spectrum unknown, float minimumSimilarity) {
    if(buffer.size() == 0){
        //cout << "you forgot to commit the spectra to the GPU memory, doing it for you!" << endl;
        commitAddedSpectra();
    }

    //cout << "searching for possible matches for " << unknown.getOriginal() << " with a minimum similarity off " << minimumSimilarity << endl;

    int numCols = MAX_ION_COUNT_FOR_SPECTRA;
    int numRows = (int) this->getLibrarySize();

    //unknown flattened spectrum
    int *unknownSpectrum = new int[numCols];
    memset(unknownSpectrum, 0, sizeof(int) * numCols);

    vector<Spectrum> tmp;
    tmp.push_back(unknown);
    util.flattenToArray(tmp, unknownSpectrum, numCols);

    //contains our results
    float *similarityScore = new float[numRows];


    //get our pre calculated buffer
    cl::Buffer buf_weightedIntensity = (cl::Buffer &&) buffer.front();

    //  create a buffer object in device memory call
    std::shared_ptr<cl::Buffer> buf_similarityScore = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                                   sizeof(float) * numRows, similarityScore);
    std::shared_ptr<cl::Buffer> buf_unknown = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR,
                               sizeof(int) * numCols, unknownSpectrum);
    std::shared_ptr<cl::Buffer> buf_numCols = std::make_shared<cl::Buffer>(*context, CL_MEM_READ_WRITE | CL_MEM_HOST_READ_ONLY | CL_MEM_COPY_HOST_PTR, sizeof(int),
                           &numCols);
    //set our kernel arguments
    this->similarityKernel->setArg(0, buf_weightedIntensity);
    this->similarityKernel->setArg(1, buf_similarityScore);
    this->similarityKernel->setArg(2, buf_unknown);
    this->similarityKernel->setArg(3, buf_numCols);

    //compute the chosen similarity
    for(auto const& queue: queues) {
        queue.enqueueNDRangeKernel(*similarityKernel, cl::NullRange, cl::NDRange(numRows));
        //write back results from device to local cpu
        queue.enqueueReadBuffer(*buf_similarityScore, CL_TRUE, 0, sizeof(float) * numRows, similarityScore);
    }

    vector<SimilarityResult> result;

    //cout << "evaluated similary scores for " << getLibrarySize() << " spectra"<< endl;

    //filter results by score
    int cnt = 0;
    for (std::vector<Spectrum>::iterator it = library.begin(); it != library.end(); ++it) {
        //cout << "score is "<< similarityScore[cnt]<<" for " << it->getSplash() << " of spectra " << it->getOriginal()<<endl;
        if (similarityScore[cnt] > minimumSimilarity) {

            //add to result vector
            result.push_back(SimilarityResult {similarityScore[cnt],it->getSplash() } );
        }
        cnt++;
    }

    //cout << "found "<< result.size() << " possible hits with a minimum similarity over " << minimumSimilarity << endl;
    //cleanup
    delete[] similarityScore;
    delete[] unknownSpectrum;

    //return the results
    return result;
}

SimilaritySearch::~SimilaritySearch() {

    library.clear();
    buffer.clear();
    queues.clear();

    similarityKernel = NULL;
    weightedIntensityKernel = NULL;

    context = NULL;
}

void SimilaritySearch::clear() {
    if(library.size() > 0) {
        library.clear();
        buffer.clear();
    }
}

