//
// Created by Gert Wohlgemuth on 6/20/17.
//

#include "../include/Spectrum.h"
#include "../include/Util.h"
#include "../../thirdparty/spectra-hash/cpp/src/splash.hpp"

Spectrum::Spectrum(const string id,const string spectra) {

    Util util;

    this->original = spectra;
    this->spectra = util.convertSpectra(spectra);
    this->splash = splashIt(spectra, '1');
    this->id = id;
}

string Spectrum::getSplash() {
    return this->splash;
}

string Spectrum::getOriginal() {
    return this->original;
}

map<int, int> Spectrum::getSpectra() {
    return this->spectra;
}

Spectrum::Spectrum(const string spectra) : Spectrum(splashIt(spectra,'1'),spectra){}

string Spectrum::getId() {
    return this->id;
}
