//
// Created by Gert Wohlgemuth on 6/20/17.
//

#include "gtest/gtest.h"
#include "../include/SimilarityResult.h"
#include "../include/SimilaritySearch.h"

TEST(SimilarityResult,evaluateResult) {
    SimilarityResult result{0.5,"test"};

    EXPECT_EQ(result.score,0.5);
    EXPECT_EQ(result.splash,"test");
}

TEST(SimilaritySearch,addLibrarySpectra) {
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 100;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
        EXPECT_EQ(search.getLibrarySize(),i+1);
    }
    EXPECT_EQ(search.getLibrarySize(),count);

    //search.commitAddedSpectra();
}

TEST(SimilaritySearch,loadLibrarySpectra){
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    search.loadLibrarySpectra("testSpectra.txt");
    EXPECT_EQ(search.getLibrarySize(),6);

    search.commitAddedSpectra();

}


TEST(SimilaritySearch,searchCosine) {
    SimilaritySearch search;

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 100;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33:3 44:0",0.5);

    //cout << count << " spectra cosine search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;
}


TEST(SimilaritySearch,searchComposite) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 100;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33:3 44:0",0.5);

    //cout << count << " spectra composite search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;

}

TEST(SimilaritySearch,searchCompositeLong) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(), 0);
    int count = 100;
    int spectraIncrement = 10000;
    int samples = 10;

    clock_t start;

    cout<< "operation duration librarySize samples" << endl;

    for(int y = 0; y < count; y++ ) {
        std::string spec = "";

        for (int i = 0; i < spectraIncrement; i++) {
            spec = "";
            for (int x = 1; x < MAX_ION_COUNT_FOR_SPECTRA; x++) {
                spec = spec + to_string(x) + ":" + to_string(rand() % 100 + 1);

                if (x < MAX_ION_COUNT_FOR_SPECTRA - 1) {
                    spec = spec + " ";
                }
            }
            //cout << spec << endl;
            search.addLibrarySpectra(spec);
        }


        double duration = 0;

        for (int i = 0; i < samples; i++) {

            start = clock();

            search.commitAddedSpectra();
            duration = duration + (clock() - start) * 1000 / CLOCKS_PER_SEC;

        }

        duration = duration/samples;

        cout<< "commit " << duration << " " << search.getLibrarySize() << " " << samples << endl;


        duration = 0;
        for (int i = 0; i < samples; i++) {

            start = clock();
            vector<SimilarityResult> result = search.search(spec, 0.5);

            duration = duration + (clock() - start) * 1000 / CLOCKS_PER_SEC;

        }

        duration = duration /samples;

        cout<< "search " << duration << " " << search.getLibrarySize() << " " << samples << endl;


    }

}
    TEST(SimilaritySearch,accurateMassSearchComposite) {
    SimilaritySearch search(COMPOSITE_SIMILARITY_KERNEL);

    EXPECT_EQ(search.getLibrarySize(),0);
    int count = 100;
    for(int i = 0; i < count; i++) {
        search.addLibrarySpectra("10:1 11:2 20:3 33:3 44:" + to_string(i));
    }

    search.commitAddedSpectra();
    clock_t start;


    start = clock();
    vector<SimilarityResult> result = search.search("10:1 11:2 20:3 33.1:1 33.2:2 44:0",0.5);

    cout << count << " spectra composite search speed: " << (clock() - start) * 1000 / CLOCKS_PER_SEC << " ms " << endl;

}
